#!/bin/bash

docker run -d \
	--name home-assistant \
	--privileged \
	--restart=unless-stopped \
	-e TZ=America/New_York \
	-v $(pwd)/config:/config:rw \
	--network=host \
	homeassistant/home-assistant:2023.3.6
