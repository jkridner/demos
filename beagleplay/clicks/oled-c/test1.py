#!/usr/bin/env python3
import os
import sys
import time
try:
    import pygame
except:
    os.system("sudo apt install -y python3-pygame")
    print("Please try again when python3-pygame is install successfully")
    quit()
from pygame.locals import *
pygame.init()
try:
    fb = open("/dev/fb1", "wb")
except:
    print("No `/dev/fb1`, so disabling and using console on `/dev/fb0`")
    os.system("sudo systemctl stop lightdm")
    os.system("sudo systemctl stop getty@tty1.service")
    fb = open("/dev/fb0", "wb")
#fb = open("test.bin", "wb")
screen = pygame.display.set_mode(size=(128,96), depth=16)
print(pygame.display.Info())

def refresh():
    if 'fb' in globals():
        fb.seek(0)
        fb.write(screen.get_buffer())
    pygame.display.update()

def dotest():
    # Red fill
    screen.fill((255,0,0))
    refresh()
    time.sleep(1)

    # White circle
    screen.fill((0,0,0))
    pygame.draw.circle(screen, (255,255,255), (64,48), 40, 1)
    refresh()
    time.sleep(1)

    # Red circle
    screen.fill((0,0,0))
    pygame.draw.circle(screen, (255,0,0), (64,48), 40, 1)
    refresh()
    time.sleep(1)

    # Green circle
    screen.fill((0,0,0))
    pygame.draw.circle(screen, (0,255,0), (64,48), 40, 1)
    refresh()
    time.sleep(1)

    # Blue circle
    screen.fill((0,0,0))
    pygame.draw.circle(screen, (0,0,255), (64,48), 40, 1)
    refresh()

while True:
    dotest()
    time.sleep(1)
